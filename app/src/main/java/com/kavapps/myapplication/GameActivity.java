package com.kavapps.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.shawnlin.numberpicker.NumberPicker;

import java.util.Random;

public class GameActivity extends AppCompatActivity {

    @BindView(R.id.imageRoulette)
    ImageView imageRoulette;

    @BindView(R.id.numberPicker)
    NumberPicker numberPicker;

    @BindView(R.id.btnStartGameId)
    Button btnStartGame;

    @BindView(R.id.textResultId)
    TextView textResult;


    private static final float FACTOR = 4.74f;
    private Random r;
    private int degree;
    private int degree_old;
    private String pickerValue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        ButterKnife.bind(this);
        r = new Random();
        degree = 0;
        degree_old = 0;
        String[] dataNumber = {"00", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
                "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23",
                "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36",};
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(dataNumber.length);
        numberPicker.setDisplayedValues(dataNumber);
        pickerValue = dataNumber[numberPicker.getValue()-1];

        numberPicker.setOnScrollListener(new NumberPicker.OnScrollListener() {
            @Override
            public void onScrollStateChange(NumberPicker picker, int scrollState) {
                if (scrollState == SCROLL_STATE_IDLE) {
                    pickerValue = dataNumber[numberPicker.getValue()-1];
                }
            }
        });


    }

    @OnClick(R.id.btnStartGameId)
    void onClickButtonStartGame(){
        startAnimation(imageRoulette);
    }



    private void startAnimation(ImageView imageView) {

        degree_old = degree % 360;
        degree = r.nextInt(3600) + 720;
        RotateAnimation rotate = new RotateAnimation(degree_old, degree, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(3600);
        rotate.setFillAfter(true);
        rotate.setInterpolator(new DecelerateInterpolator());
        rotate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                textResult.setVisibility(View.INVISIBLE);
                numberPicker.setEnabled(false);
                btnStartGame.setTextColor(getApplicationContext().getResources().getColor(R.color.gray));
                btnStartGame.setClickable(false);
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                showWinOrLose(degree);
                numberPicker.setEnabled(true);
                btnStartGame.setTextColor(getApplicationContext().getResources().getColor(R.color.white));
                btnStartGame.setClickable(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        imageView.startAnimation(rotate);
    }


    private String currentNumber(int degrees) {
        String text = "";
        if (degrees >= (FACTOR * 1) && degrees < (FACTOR * 3)) {
            text = "27";
        }
        if (degrees >= (FACTOR * 3) && degrees < (FACTOR * 5)) {
            text = "10";
        }
        if (degrees >= (FACTOR * 5) && degrees < (FACTOR * 7)) {
            text = "35";
        }
        if (degrees >= (FACTOR * 7) && degrees < (FACTOR * 9)) {
            text = "29";
        }
        if (degrees >= (FACTOR * 9) && degrees < (FACTOR * 11)) {
            text = "12";
        }
        if (degrees >= (FACTOR * 11) && degrees < (FACTOR * 13)) {
            text = "8";
        }
        if (degrees >= (FACTOR * 13) && degrees < (FACTOR * 15)) {
            text = "19";
        }
        if (degrees >= (FACTOR * 15) && degrees < (FACTOR * 17)) {
            text = "31";
        }
        if (degrees >= (FACTOR * 17) && degrees < (FACTOR * 19)) {
            text = "18";
        }
        if (degrees >= (FACTOR * 19) && degrees < (FACTOR * 21)) {
            text = "6";
        }
        if (degrees >= (FACTOR * 21) && degrees < (FACTOR * 23)) {
            text = "21";
        }
        if (degrees >= (FACTOR * 23) && degrees < (FACTOR * 25)) {
            text = "33";
        }
        if (degrees >= (FACTOR * 25) && degrees < (FACTOR * 27)) {
            text = "16";
        }
        if (degrees >= (FACTOR * 27) && degrees < (FACTOR * 29)) {
            text = "4";
        }
        if (degrees >= (FACTOR * 29) && degrees < (FACTOR * 31)) {
            text = "23";
        }
        if (degrees >= (FACTOR * 31) && degrees < (FACTOR * 33)) {
            text = "35";
        }
        if (degrees >= (FACTOR * 33) && degrees < (FACTOR * 35)) {
            text = "14";
        }
        if (degrees >= (FACTOR * 35) && degrees < (FACTOR * 37)) {
            text = "2";
        }
        if (degrees >= (FACTOR * 37) && degrees < (FACTOR * 39)) {
            text = "0";
        }
        if (degrees >= (FACTOR * 39) && degrees < (FACTOR * 41)) {
            text = "28";
        }
        if (degrees >= (FACTOR * 41) && degrees < (FACTOR * 43)) {
            text = "9";
        }
        if (degrees >= (FACTOR * 43) && degrees < (FACTOR * 45)) {
            text = "26";
        }
        if (degrees >= (FACTOR * 45) && degrees < (FACTOR * 47)) {
            text = "30";
        }
        if (degrees >= (FACTOR * 47) && degrees < (FACTOR * 49)) {
            text = "11";
        }
        if (degrees >= (FACTOR * 49) && degrees < (FACTOR * 51)) {
            text = "7";
        }
        if (degrees >= (FACTOR * 51) && degrees < (FACTOR * 53)) {
            text = "20";
        }
        if (degrees >= (FACTOR * 53) && degrees < (FACTOR * 55)) {
            text = "32";
        }
        if (degrees >= (FACTOR * 55) && degrees < (FACTOR * 57)) {
            text = "17";
        }
        if (degrees >= (FACTOR * 57) && degrees < (FACTOR * 59)) {
            text = "5";
        }
        if (degrees >= (FACTOR * 59) && degrees < (FACTOR * 61)) {
            text = "22";
        }
        if (degrees >= (FACTOR * 61) && degrees < (FACTOR * 63)) {
            text = "34";
        }
        if (degrees >= (FACTOR * 63) && degrees < (FACTOR * 65)) {
            text = "15";
        }
        if (degrees >= (FACTOR * 65) && degrees < (FACTOR * 67)) {
            text = "3";
        }
        if (degrees >= (FACTOR * 67) && degrees < (FACTOR * 69)) {
            text = "24";
        }
        if (degrees >= (FACTOR * 69) && degrees < (FACTOR * 71)) {
            text = "36";
        }
        if (degrees >= (FACTOR * 71) && degrees < (FACTOR * 73)) {
            text = "13";
        }
        if (degrees >= (FACTOR * 73) && degrees < (FACTOR * 75)) {
            text = "1";
        }
        if ((degrees >= (FACTOR * 75) && degrees < 360) || (degrees >= 0 && degrees < (FACTOR * 1))) {
            text = "00";
        }

        return text;
    }


    private void showWinOrLose(int degree) {
        textResult.setVisibility(View.VISIBLE);
        if (pickerValue.equals(currentNumber(360 - (degree % 360)))){
            textResult.setText(getResources().getText(R.string.win));
        }else {
            textResult.setText(getResources().getText(R.string.lose));
        }

    }

}
